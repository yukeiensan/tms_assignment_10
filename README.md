# Dependencies
If you're running the main.py file exec (if you run the executable you can skip this step):
- `pip install argsparse`

# Usage
```
usage: matching.exe [-h] [-o OUTPUT] input

Exchange matching software

positional arguments:
  input                 Input file

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Output file
```