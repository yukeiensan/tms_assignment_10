from enum import Enum, auto


class AutoName(Enum):
    def _generate_next_value_(name, start, count, last_values):
        return name


class Type(AutoName):
    BUY = auto()
    SELL = auto()


class Action:
    def __init__(self, row):
        self.id = int(row["id"])
        self.account = row["account"]
        self.pair = row["pair"]
        self.type = Type(row["action"])
        self.price = float(row["price"])
        self.matched = None

    def is_matched(self):
        if self.matched:
            return False
        else:
            return True

    def can_match(self, match):
        if self.type == Type("BUY") and self.pair == match.pair and self.account != match.account and self.price >= match.price:
            self.matched = match
            return True
        else:
            return False
