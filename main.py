#!/usr/bin/env python3

import argparse
from Action import Action, Type
import math
import csv


# Global Variables
EXPECTED_CSV = ["id", "account", "pair", "action", "price"]


# Project functions
def parsing_arguments():
    parser = argparse.ArgumentParser(description="Exchange matching software")
    parser.add_argument("input", help="Input file")
    parser.add_argument("-o", "--output", help="Output file", default="matches.csv")
    args = parser.parse_args()

    return args


def check_csv_header(headers):
    validate = []

    for expected_header in EXPECTED_CSV:
        for header in headers:
            if expected_header == header:
                validate.append(0)
                break

    if len(validate) != len(EXPECTED_CSV):
        return False

    return True


def csv_to_object(csv):
    obj_list = []
    headers = []
    for index, row in enumerate(csv):
        obj = {}
        if index == 0:
            if not check_csv_header(row):
                print("Invalid CSV header given")
                exit(84)
            headers = row
        else:
            for header_index, header in enumerate(headers):
                obj[header] = row[header_index]
            obj_list.append(obj)

    return obj_list


def read_csv_file(input):
    actions = []

    file = open(input)
    csv_rows = list(csv.reader(file, skipinitialspace=True))
    rows = csv_to_object(csv_rows)
    for row in rows:
        actions.append(Action(row))

    return actions


def split_actions(actions):
    buyers = []
    sellers = []

    for action in actions:
        if action.type == Type("BUY"):
            buyers.append(action)
        elif action.type == Type("SELL"):
            sellers.append(action)

    return buyers, sellers


def get_action_index(list, id):
    for index, elem in enumerate(list):
        if elem.id == id:
            return index

    return -1


def match_actions(actions, buyers, sellers):
    for buyer in buyers:
        for seller in sellers:
            if actions[buyer.id].can_match(seller):
                actions[seller.id].matched = buyer
                sellers.pop(get_action_index(sellers, seller.id))
                break

    return actions


def print_actions(actions, output):
    file = open(output, "w+")

    for header in EXPECTED_CSV:
        file.write(header + ",")
    file.write("match\n")

    for action in actions:
        file.write(str(action.id) + "," + str(action.account) + "," + str(action.pair) + "," + str(action.type)
                   + "," + str(action.price) + ",")
        if action.matched:
            file.write(str(action.matched.id) + "\n")
        else:
            file.write("REJECTED\n")


def main():
    args = parsing_arguments()

    actions = read_csv_file(args.input)
    buyers, sellers = split_actions(actions)
    actions = match_actions(actions, buyers, sellers)

    print_actions(actions, args.output)


if __name__ == "__main__":
    main()
